import React, { useState, useEffect } from 'react';
import { Button, Table, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function Cart() {
  const [orders, setOrders] = useState([]);
  const { _id: userId } = useParams();

  useEffect(() => {
    fetch(`https://e-commerce-backendapi.onrender.com/users/${userId}/cart`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setOrders(data);
    })
  }, []);

  function removeProduct(orderIndex, productIndex) {
    const updatedOrders = [...orders];
    updatedOrders[orderIndex].formattedProducts.splice(productIndex, 1);
    setOrders(updatedOrders);
  }

  function renderProducts(order) {
    if (!order.formattedProducts || !order.formattedProducts.length) return null;
    return (
      <Table striped bordered hover key={order._id}>
        <thead>
          <tr>
            <th colSpan="3">Order Id: {order._id}</th>
          </tr>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {order.formattedProducts.map((product, index) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.price}</td>
              <td>{product.quantity}</td>
              <td>
                <Button
                  className="my-2 rounded bg-warning"
                  onClick={() => removeProduct(orders.indexOf(order), index)}
                >
                  Remove
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td colSpan="2">Sub Total:</td>
            <td>{order.totalAmount}</td>
          </tr>
        </tfoot>
      </Table>
    );
  }

  function handleCheckout() {
    console.log('Checkout button clicked!');
  }

  const totalAmount = orders.reduce((acc, order) => acc + order.totalAmount, 0);

  return (
    <Row className="cart-page mt-md-5 pt-md-5">
      <h1 className="text-center">Review your order!</h1>
      <div>
          {orders.map(order => renderProducts(order))}
          {orders.length > 0 &&
            <div className="d-flex justify-content-end">
              <h4>Total: PHP {totalAmount} </h4>
            </div>
          }
          {orders.length > 0 && // added condition to render checkout button only when there are orders
            <div className="text-center">
              <Button className="ml-5 bg-warning" variant="primary" onClick={handleCheckout}>
                Checkout
              </Button>
            </div>
          }
        </div>

    </Row>
  );
  
}
