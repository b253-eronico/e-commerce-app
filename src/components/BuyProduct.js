import React, { useState, useEffect, useContext } from 'react';
import { Row, Container, Col, Button, Card } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Cart from './Cart'

import 'bootstrap/dist/css/bootstrap.min.css';

export default function SpecificProduct() {

  const navigate = useNavigate();

  const { user } = useContext(UserContext); 

  const [cartItems, setCartItems] = useState([]);
  const [quantity, setQuantity] = useState(0);
  const [price, setPrice] = useState(0);
  const [productName, setProductName] = useState("");
  const { productId } = useParams();
  const [product, setProduct] = useState({});
  const userId = user.id; 





  useEffect(() => {
    fetch(`https://e-commerce-backendapi.onrender.com/products/${productId}`, {
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setProductName(data.name) ;
        setPrice(data.price)
      });
  }, [productId]);

  console.log(userId, productId, productName, price, quantity)



  function handleAddToCart(e) {

    e.preventDefault();
   
      
      navigate(`/${userId}/cart`)

     
      fetch(`https://e-commerce-backendapi.onrender.com/products/${productId}/order`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          userId: userId,
          productId: productId,
          productName: productName,
          price: price,
          quantity: quantity
        })
      }).then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      }).then(data => {
        console.log('Order saved:', data);
        
      }).catch(error => {
        console.error('Error saving order:', error);
      });
  
    } 
   
 
  

  function handleQuantityChange(e) {
    const value = e.target.value;
    if (value > 0) {
      setQuantity(Number(value));
    }
  }

  function handleQuantityIncrement() {
    setQuantity(quantity + 1);
  }

  function handleQuantityDecrement() {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  }
  
  return (
    
    
      <Row className="mt-5 pt-5 justify-content-center buy-page">
          <h1 className="text-center">Buy Now! </h1>
        <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
          <Card className="product-card my-3 p-3 rounded">
            <Card.Body>
              <Card.Title as="div">
                <strong>{product.name}</strong>
              </Card.Title>
              <Card.Text>
                <div className="my-3">{product.description}</div>
              </Card.Text>
              <Card.Text>
                <div className="my-3">Price: {product.price}</div>
              </Card.Text>
              <div>
                <div className="d-flex align-items-center justify-content-between mt-3">
                  <span>Quantity:</span>
                  <div className="d-flex align-items-center">
                    <Button
                      className="mx-2"
                      variant="outline-primary"
                      onClick={handleQuantityDecrement}
                    >
                      -
                    </Button>
                    <input
                      type="number"
                      className="form-control"
                      value={quantity}
                      onChange={handleQuantityChange}
                      min="1"
                    />
                    <Button
                      className="mx-2"
                      variant="outline-primary"
                      onClick={handleQuantityIncrement}
                    >
                      +
                    </Button>
                  </div>  
                </div>
                  <>
                    <Card.Text>
                      <div className="my-3">Subtotal: {product.price * quantity}</div>
                    </Card.Text>
                  </> 
                <Button
                  className="my-2 rounded"
                  onClick={(e) => handleAddToCart(e)}
                >
                 
                  Add to Cart
                </Button>
              </div>       
            </Card.Body>
          </Card>
        </Col>
      </Row>
      
  


  );
  }  

