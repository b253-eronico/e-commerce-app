import { useContext, useEffect } from 'react';
import {  Navbar, Nav, Dropdown, Button } from "react-bootstrap"
import { Link, NavLink, useNavigate } from "react-router-dom";
import UserContext from '../UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

export default function AppNavbar(){

  const { user, unsetUser } = useContext(UserContext);

  const navigate = useNavigate();

	

  return (
    <Navbar expand="lg" className="navbar-black"  fixed="top">
			
      <Navbar.Brand as={ Link } to ="/" className="kingsman"><img src="/logo.png" width="40" height="40"/>{' '}KingsMan</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
	  <Nav className="ms-auto nav-bar">
  {user.isAdmin === true ? (
    <>
      <Nav.Link as={NavLink} to="/admin" className="text-light">
        Dashboard
      </Nav.Link>
      
      <Nav.Link as={NavLink} to="/logout" className="text-light">
       Logout
      </Nav.Link>
    </>
  ) : (
    <>
      <Nav.Link as={NavLink} to="/" className="text-light">
        Home
      </Nav.Link>
      <Nav.Link as={NavLink} to="/products" className="text-light">
        Products
      </Nav.Link>
      {user.id !== null && (
        <Dropdown drop="start" className="dropdown">
          <Dropdown.Toggle variant="link" id="dropdown-basic">
            <img src="./user.png" className="text-warning" width="30" height="30"></img>
          </Dropdown.Toggle>
          <Dropdown.Menu className="text-warning">
            <Dropdown.Item as={NavLink} to={`/${user.id}/cart`} className="text-warning dropdown-item">
              Cart
            </Dropdown.Item>
            <Dropdown.Item as={NavLink} to='/logout' className="text-warning dropdown-item"> 
              Logout
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      )}
      {user.id === null && (
        <>
          <Nav.Link as={NavLink} to="/login" className="text-light">
            Login
          </Nav.Link>
          <Nav.Link as={NavLink} to="/register" className="text-light">
            Register
          </Nav.Link>
        </>
      )}
    </>
  )}
</Nav>

      </Navbar.Collapse>
			
    </Navbar>
  )
}
