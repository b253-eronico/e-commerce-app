import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useParams, Link } from 'react-router-dom';
import { Container, Row, Col, Table, Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


export default function AllProduct() {

const { id } = useParams();

  const [products, setProducts] = useState([]);
  const [updateStatus, setUpdateStatus] = useState([]);

 

  useEffect(() => {



    fetch(`https://e-commerce-backendapi.onrender.com/products/all`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, [products]);

  

 const toggleStatus = async (productId) => {

  // productId.preventDefault();

  try {
    const response = await fetch(`https://e-commerce-backendapi.onrender.com/products/${productId}/status`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return data
    })
    // const updatedProduct = await response.json();
    // const updatedProducts = products.map((product) => {
    //   if (product.id === updatedProduct.id) {
    //     console.log(updatedProduct)

    //     return updatedProduct;
        
    //   }
    //   return product;
      
    // });
    // setUpdateStatus(updatedProducts);
  } catch (error) {
  
  }
};



  return (
    <Container fluid className="all-page">
      <Row className="text-center d-flex flex-column  align-items-center">
        <h1>Gear Inventory</h1>
        <Col className="col-lg-8">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>isActive</th>
                <th>Actions</th>
                <th>Activate/Deactivate</th>
              </tr>
            </thead>
            <tbody>
                {products.map((item) => (
                <tr key={item._id}>
                  <td>{item.name}</td>
                  <td>{item.description}</td>
                  <td>{item.price}</td>
                  <td>{item.isActive ? 'Yes' : 'No'}</td>
                  
                  <td>
                  <Link to={`/updateproduct/${item._id}`}>
  <Button variant="primary">
    Update
  </Button>
</Link>
                </td>
                <td>
                    <Button variant="primary" onClick={() => toggleStatus(item._id)}>
                      {item.isActive !== false ? 'Archive' : 'Activate'}
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  )}


