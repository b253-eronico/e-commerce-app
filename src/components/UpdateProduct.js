import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useParams } from 'react-router-dom';
import { Container, Row, Col, Table, Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from "sweetalert2";

export default function UpdateProduct() {
  const [name, setName] = useState('');
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const [productName, setProductName] = useState('');
  const [products, setProducts] = useState([]);
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');


  function handleSubmit(e) {
    e.preventDefault();
  
    fetch(`https://e-commerce-backendapi.onrender.com/products/${productId}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
        name: name,
        price: price,
        description: description,
      }),
      
    })
      .then((res) => res.json(productId))
      .then((data) => {
        console.log("Data before updating products:", data);
  
        if (data.error) {
          Swal.fire({
            title: "Updating Product Failed",
            icon: "error",
            text: "data.error"
          });
        } else {
          Swal.fire({
            title: "Succesfully updated",
            icon: "success",
            text: "Product has been updated.",
          }, []);
  
          setName("");
          setPrice("");
          setDescription("");
  
          // fetch the updated list of products
          fetch("https://e-commerce-backendapi.onrender.com/products/all")
            .then((res) => res.json())
            .then((data) => {
              setProducts(data);
            })
            .catch((error) => console.log(error));
        }
      })
      .catch((error) => console.log(error));
  }
  
  
  

        function handleInputChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.value;

    switch (name) {
      case "name":
        setName(value);
        break;
      case "price":
        setPrice(value);
        break;
      case "description":
        setDescription(value);
        break;
      default:
        break;
    }
  }

  return (
    <Row className="update-page mt-5 pt-5">
      <Col>
        <h1>Update Products</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              name="name"
              value={name}
              onChange={handleInputChange}
            />
          </Form.Group>
          <Form.Group controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Enter product description"
              name="description"
              value={description}
              onChange={handleInputChange}
            />
          </Form.Group>
          <Form.Group controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter product price"
              name="price"
              value={price}
              onChange={handleInputChange}
            />
          </Form.Group>
          <Button variant="primary" type="submit">Update Product</Button>
        </Form>
      </Col>
    </Row>
  );
}
