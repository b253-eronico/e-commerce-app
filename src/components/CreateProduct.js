import React, { useState, useContext } from 'react';
import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



import 'bootstrap/dist/css/bootstrap.min.css';


export default function CreateProduct() {

    const { user } = useContext(UserContext);

  const [product, setProduct] = useState({
    name: '',
    price: '',
    description: ''
  });

  const productChange = (e) => {
    const { name, value } = e.target;
    setProduct(prevProduct => ({
      ...prevProduct,
      [name]: value
    }));
  }

  const productSubmit = (e) => {
    e.preventDefault();
  
    if (!user || !user.isAdmin) {
      // Display an error message if the user is not an admin
      Swal.fire({
        title: 'Not Allowed',
        icon: 'error',
        text: 'You are not authorized to create a product.'
      });


    return;
    } else if(user.isAdmin !== false ) {
        fetch(`https://e-commerce-backendapi.onrender.com/products/create`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: product.name,
        description: product.description,
        price: product.price
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
  
      if (data !== false) {
        setProduct({
          name: '',
          description: '',
          price: ''
          
        });
  
        Swal.fire({
          title: 'Product creation successful!',
          icon: 'success',
        });
      } else {
        Swal.fire({
          title: 'Something wrong',
          icon: 'error',
          text: 'Please try again.'   
        })
      }
    })
    }

  }
  

    {
        return (
        
            <Row className="text-center d-flex flex-column  align-items-center create-page">
            
                <Col className="col-lg-6 border rounded mt-5">
                    <Form onSubmit={productSubmit}>
                        <Form.Group controlId="productName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" value={product.name} onChange={productChange} placeholder="Enter product name" required />
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={10} name="description" value={product.description} onChange={productChange} placeholder="Enter product description" required />
                        </Form.Group>

                        <Form.Group controlId="productPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" name="price" value={product.price} onChange={productChange} placeholder="Enter product price" required />
                        </Form.Group>

                        <Button variant="warning" type="submit" className="my-3 text-light border-white">
                        Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
       
        )
    } 
}


