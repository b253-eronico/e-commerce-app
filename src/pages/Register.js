import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row } from 'react-bootstrap';
import { Navigate,  useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	
	
	
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	
	const [isActive, setIsActive] = useState(false);

	const [ registerSuccess, setRegisterSuccess ] = useState(false);

	
	function registerUser(e) {
		
		e.preventDefault();

		fetch(`https://e-commerce-backendapi.onrender.com/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){

		    	Swal.fire({
		    		title: 'Duplicate email found',
		    		icon: 'error',
		    		text: 'Kindly provide another email to complete the registration.'	
		    	});

		    } else {

	            fetch(`{process.env.REACT_APP_API_URL}/users/register`, {
	                method: "POST",
	                headers: {
	                    'Content-Type': 'application/json'
	                },
	                body: JSON.stringify({
	                    
	                    email: email,
	                    password: password1
	                })
	            })
	            .then(res => res.json())
	            .then(data => {

	                console.log(data);

	                if (data === true) {

	                    
	                
	                    setEmail('');
	                    setPassword1('');
	                    setPassword2('');

	                    Swal.fire({
	                        title: 'Registration successful',
	                        icon: 'success',
	                        text: 'Welcome to Zuitt!'
	                    });

	                    
	                    navigate("/login");

	                } else {

	                    Swal.fire({
	                        title: 'Something wrong',
	                        icon: 'error',
	                        text: 'Please try again.'   
	                    });
	                }

				});
	        }
	    })
	}
	

	useEffect(() => {

	    
	    if(( email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [ email, password1, password2]);

	return registerSuccess !== false ? (
			
		<Navigate to="/login"/>
	)
		:
	(
		// 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
		<Row className="register-page mt-5 pt-5">
			<h1 className="text-center text-light">Come and Join US!</h1>
			<div className="d-flex flex-column  align-items-center">
				<Form onSubmit={(e) => registerUser(e)} className="border p-3 rounded col-lg-6 register-form">
				<Form.Group className="mb-3" controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email"
						value={ email }
						onChange={e => setEmail(e.target.value)}
						required/>
					<Form.Text className="text-muted">
					We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group className="mb-3" controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password"
						value={ password1 }
						onChange={e => setPassword1(e.target.value)} 
						required/>
				</Form.Group>
				<Form.Group className="mb-3" controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Verify Password"
						value={ password2 }
						onChange={e => setPassword2(e.target.value)}  
						required/>
				</Form.Group>

				{ isActive ?
						<Button variant="warning" type="submit" id="submitBtn" className="mx-auto d-block w-50">
						Submit
						</Button>
						:
						<Button variant="light" type="submit" id="submitBtn" disabled className="mx-auto d-block w-50">
						Submit
						</Button>
					}
				</Form>
			</div>	
	</Row>
	)
}

