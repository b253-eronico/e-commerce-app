import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


import 'bootstrap/dist/css/bootstrap.min.css';

export default function Home() {
  return (
    
	
		<Row className="home-page">
			<Col className="mt-5 pt-5 mx-5">
				<div className='mt-5 pt-5'>
					<h1>Welcome to KingsMan!</h1>
					<h5>Manners.Maketh.Man.</h5>
				</div>
				<div className='mt-2 text-end'>
					<p>"Strangers gathered and became a guild"</p>
					<p>Join us and enjoy pvp, click here to <Link to="/products">view</Link> the server's available gears.</p>
				</div>
			</Col>
			<Col className="mt-5 pt-5">
				<div className="mt-5 pt-3">
					<h5 className='text-center'>Check out one of our Castle Siege experiences:</h5>
				</div>
			<div  className="mt-3 pt-5">
				<iframe width="850" height="550" src="https://www.youtube.com/embed/pWXqtGmYFuE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
			</div>
			</Col>
		</Row>		
	

  )
}
