import { useState, useEffect, useContext, React } from 'react'; //hook

import { Form, Button, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import 'bootstrap/dist/css/bootstrap.min.css';

import UserContext from '../UserContext'

export default function Login() {

	
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	
	const [isActive, setIsActive] = useState(false);

	


	
	function loginUser(e) {
		
		e.preventDefault();

		
		fetch('https://e-commerce-backendapi.onrender.com/users/login',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)

			
			if(typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
			
				retrieveUserDetails(data.access);

				// Swal.fire({
				// 	title: "Login Successful",
				// 	icon: "success",
				// 	text: "Welcome to Zuitt!"
				// })
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login credentials and try again"
				})
			}
		});

		setUser({ email: localStorage.getItem('email')})

		// Clear input fields after submission
		setEmail('');
		setPassword('');

	}

	const retrieveUserDetails = (token) => {

		
		fetch('https://e-commerce-backendapi.onrender.com/users/profile', {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			localStorage.setItem("id", data._id)
			localStorage.setItem("isAdmin", data.isAdmin)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	}

	

	useEffect(() => {
		
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	
		
		return (
			user.id !== null ? (
				user.isAdmin === true ? (
					<Navigate to="/admin" />
				) : (
					<Navigate to="/" />
				)
			) 
			
			: 
			
			(
				<Row className="login-page mt-5 pt-5">
					<div className=" mt-md-5 pt-md-5">
						<div className=" d-flex flex-column align-items-center mt-md-5 pt-md-5 px-md-5 mx-md-5">
							<Form onSubmit={(e) => loginUser(e)} className="border rounded col-lg-8 px-5 login-form">
								<div className="text-center mt-4">
									<h1>Login</h1>
								</div>
								<Form.Group className="mt-5 mx-md-5 px-md-5" controlId="userEmail">
									<Form.Label>Email address</Form.Label>
									<Form.Control 
										type="email" 
										placeholder="Enter email"
										value={ email }
										onChange={ e => setEmail(e.target.value) } 
										required
									/>
								</Form.Group>
								<Form.Group className="mt-5 mx-md-5 px-md-5" controlId="password1">
									<Form.Label>Password</Form.Label>
									<Form.Control 
										type="password" 
										placeholder="Password"
										value={ password }
										onChange={ e => setPassword(e.target.value) }  
										required
									/>
								</Form.Group>
								{ isActive ? (
									<div className="text-center">
										<Button variant="warning" type="submit" id="submitBtn" className=" w-50 mt-5 mx-md-5 px-md-5 mb-2">
											Login
										</Button>
									</div>
								) : (
									<div className="text-center">
										<Button variant="light" type="submit" id="submitBtn" disabled className="w-50 mt-5 mx-md-5 px-md-5 mb-2">
											Login
										</Button>
									</div>
								)}
							</Form>
							<p>Don't have an account yet? <a href="/register">Click here</a> to register</p>
						</div>
					</div>
				</Row>
			)
		)
		
}


