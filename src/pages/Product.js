import React, { useState, useEffect } from 'react';
import { Row, Container, Col, Button, Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

export default function Product() {

  const { id } = useParams();

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://e-commerce-backendapi.onrender.com/products/active`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
   
 
      <Row className="mt-5 pt-5 px-5 justify-content-center product-page">
      <h1 className="mt-5 pt-5 text-center">Choose your Class and gear Build:</h1>
        {products.map((product) => (
          <Col key={product._id} sm={12} md={6} lg={4} xl={4}>
            <Card className="px-2 mx-4 product-card rounded">
              <Card.Body>
                <Card.Title as="div">
                  <strong>{product.name}</strong>
                </Card.Title>
                <Card.Text>
                  <div>Price: PHP{' '}{product.price}</div>
                </Card.Text>
                <Button href={`/products/${product._id}`} className="rounded bg-warning border-light">
                  <p className="m-1">View set seed options</p>
                </Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    
  );
};
